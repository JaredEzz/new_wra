import 'package:flutter/material.dart';


DecorationImage image1 = DecorationImage(
    image: new NetworkImage("https://cdn.topgolf.com/assets/galleries/11551/2131_hitting-bays-night-topgolf-centennial-01.jpg"),
    fit: BoxFit.cover
);

DecorationImage image2 = DecorationImage(
    image: new NetworkImage("https://nsclibrary.org/wp-content/uploads/2018/04/movie.jpg"),
    fit: BoxFit.cover
);

//
DecorationImage image3 = DecorationImage(
    image: new NetworkImage("https://www.kamloopshikingclub.net/wp-content/uploads/2018/04/what-to-wear-hiking-1170x609-392x272.jpg"),
    fit: BoxFit.cover
);

//Bar
DecorationImage image4 = DecorationImage(
    image: new NetworkImage("https://cdn.vox-cdn.com/thumbor/HBFgV_aM98H0dqrNCQYwzTfs8x4=/0x0:8256x6191/1200x675/filters:focal(4785x2414:6105x3734)/cdn.vox-cdn.com/uploads/chorus_image/image/55930313/Photo_Jul_26__12_33_33.0.jpg"),
    fit: BoxFit.cover
);

//Water Park
DecorationImage image5 = DecorationImage(
    image: new NetworkImage("https://upload.wikimedia.org/wikipedia/commons/f/f9/Munsu_Water_Park_-_Pyongyang%2C_North_Korea_%2811511854045%29.jpg"),
    fit: BoxFit.cover
);

//Night Life Comedy
DecorationImage image6 = DecorationImage(
    image: new NetworkImage("https://cbssanfran.files.wordpress.com/2012/04/nightlife-music-comedy-rooster-t-feathers-comedy-club.jpg"),
    fit: BoxFit.cover
);
